﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit4_Style
{

    public partial class MyResource : ContentPage
    {
        public MyResource()
        {
            InitializeComponent();


            Entry btn = new Entry();



            Style s = new Style(typeof(Entry));
            s.Setters.Add(new Setter() { Property = Entry.TextColorProperty, Value = Color.FromHex("#0000fa") });
            s.Setters.Add(new Setter() { Property = Entry.FontSizeProperty, Value = 12 });
            btn.Style = s;


        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            
        }
    }
}
